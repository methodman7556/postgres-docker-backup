# postgres-docker-backup

## Configuration

### Environment Variables

#### AWS_ENDPOINT_URL

Obtain from your object storage providers dashboard

#### AWS_ACCESS_KEY_ID

Obtain from your object storage providers dashboard

#### AWS_SECRET_ACCESS_KEY

Obtain from your object storage providers dashboard

### BACKUP_FILENAME

Filename to be used for generated backup files

#### PGHOST

PostgreSQL host

#### PGUSER

PostgreSQL user

#### PGPASSWORD

PostgreSQL password