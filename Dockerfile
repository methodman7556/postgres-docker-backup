FROM python:3.10.8-alpine

RUN apk add --no-cache curl postgresql-client

WORKDIR /opt/postgresql-docker-backup

COPY backup.py .

COPY requirements.txt .

RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "backup.py" ]