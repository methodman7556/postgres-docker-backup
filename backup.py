import boto3
import os
import time
from sh import pg_dumpall


seconds_since_epoch = int(time.time())
BACKUP_FILENAME = os.getenv("BACKUP_FILENAME", f"postgres-backup-file")
BACKUP_FILENAME = f"{BACKUP_FILENAME}-{seconds_since_epoch}.sql"

pg_dumpall("--file", BACKUP_FILENAME)

AWS_ENDPOINT_URL = os.getenv("AWS_ENDPOINT_URL")
AWS_S3_BUCKET = os.getenv("AWS_S3_BUCKET")

s3 = boto3.resource('s3',
  endpoint_url = AWS_ENDPOINT_URL,
)


bucket = s3.Bucket(AWS_S3_BUCKET)
bucket.upload_file(BACKUP_FILENAME, BACKUP_FILENAME)
